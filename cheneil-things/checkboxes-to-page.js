// OPTION 1:
// - use query params to show items

const checked = ["item 1", "item 2", "item 3"];

const onNavParams = () => {
  history.pushState(null, null, "?checked=[" + checked + "]");
};

// and on new page read from params

const onLoadParams = () => {
  const checked = queryParams.checked;
  if (checked) {
    checked.forEach((item) => {
      // create an list item for each item that was checked
      const listItem = document.createElement("li");
      listItem.innerText = item;
      // append the list item to the list
      document.querySelector("#list").appendChild(listItem);
    });
  }
};

// OPTION 2:
// - local storage to maintain state

// original page
const onNav = () => {
  localStorage.setItem("checked", JSON.stringify(checked));
};

// new page

const onLoad = () => {
  const checked = JSON.parse(localStorage.getItem("checked"));
  if (checked) {
    checked.forEach((item) => {
      // create an list item for each item that was checked
      const listItem = document.createElement("li");
      listItem.innerText = item;
      // append the list item to the list
      document.querySelector("#list").appendChild(listItem);
    });
  }
};

// OPTION 3:
// - you are now wandering into single page app land
// - react, vue, would be good places to start
// - you can maintain state a bit more easily
